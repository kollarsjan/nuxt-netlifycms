export const state = () => ({
  blogPosts: [],
  pages: [],
  navigation: {}
})

export const mutations = {
  setBlogPosts(state, list) {
    state.blogPosts = list
  },
  setPages(state, list) {
    state.pages = list
  },
  setNavigation(state, obj) {
    state.navigation = obj
  }
}

export const actions = {
  async nuxtServerInit({ commit }) {
    const blogFiles = await require.context('~/assets/content/blog/', false, /\.json$/)
    const blogPosts = blogFiles.keys().map(key => {
      const res = blogFiles(key)
      res.slug = key.slice(2, -5)
      return res
    })
    const pagesFiles = await require.context('~/assets/content/pages/', false, /\.json$/)
    const pages = pagesFiles.keys().map(key => {
      const res = pagesFiles(key)
      res.slug = key.slice(2, -5)
      return res
    })
    const navigationFile = await require('~/assets/content/settings/navigation.json')
    commit('setBlogPosts', blogPosts)
    commit('setPages', pages)
    commit('setNavigation', navigationFile)
  }
}
